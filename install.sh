#!/bin/bash
DOTFILE_PATH=$HOME/.config/coderv2/dotfiles

ln -s $DOTFILE_PATH/.gitconfig $HOME/.gitconfig
ln -s $DOTFILE_PATH/.bash_aliases $HOME/.bash_aliases
