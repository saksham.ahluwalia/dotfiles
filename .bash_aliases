alias gs="git status"
alias gp="git push"
alias gpf="git push -f"
alias gaa="git add ."
alias gcm="git commit -m"
alias gco="git checkout"
alias gcb="git checkout -b"
alias gb="git branch"
alias gbd="git branch -d"
alias gfp="git fetch && git pull"

alias nv="node --version"
alias nu="nvm use v18.15.0"
alias ni="nvm install v18.15.0"
alias yi="npm install --global yarn"

alias cdf="cd monorepo/apps/ui-web-travel/"
alias s="yarn start"
alias t="yarn test"
alias c="clear"
alias b="/home/coder/.pyenv/versions/3.9.9/bin/black"